# forkio-step-Barskiy-Spirkin-project

### Список использованных технологий:
1. node package managment
2. gulp
3. node.js
4. javascript
5. html css

##

### Состав участников проекта:
- Барский Глеб
- Спиркин Андрей

##

### Выполнение заданий:
- Барский Глеб:
    - Сверстал блоки Revolutionary Editor и секции Here is what you get, Fork Subscription Pricing;
    - Создал и оформил файл README.MD;
    - Создал, управлял репозиторием
    - Загрузил проект на GitLab Pages
- Спиркин Андрей: 
    - Сверстал шапку сайта с верхним меню (включая выпадающее меню при малом разрешении экрана) и секцию People Are Talking About Fork;
    - Полностью сделал всю сборку